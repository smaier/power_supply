#include <ctime>
#include <iostream>
#include <stdio.h>
#include <unistd.h>

#include "ITPowerSupplyInterface.h"

//========================================================================================================================
ITPowerSupplyInterface::ITPowerSupplyInterface(int serverPort, std::string configFileName) : TCPServer(serverPort, 10) { fHandler.readSettings(configFileName, fDocSettings); }

ITPowerSupplyInterface::ITPowerSupplyInterface(int serverPort) : TCPServer(serverPort, 10) { std::cout << "Ready to receive datas!" << std::endl; }

//========================================================================================================================
ITPowerSupplyInterface::~ITPowerSupplyInterface(void) { std::cout << __PRETTY_FUNCTION__ << " DESTRUCTOR" << std::endl; }

//========================================================================================================================
std::string ITPowerSupplyInterface::interpretMessage(const std::string& buffer)
{
    std::lock_guard<std::mutex> theGuard(fMutex);

    std::cout << __PRETTY_FUNCTION__ << " Message received from Ph2ACF: " << buffer << std::endl;
    
    std::map<std::string, std::string> valuesMap;
    try
    {
        valuesMap = createVariablesMap(buffer);
    }
    catch(const std::runtime_error& rte)
    {
        std::cout << "Entering error" << std::endl;
        std::cerr << rte.what() << '\n';
        throw std::runtime_error(rte.what());
    }
    if(buffer == "Initialize") // Changing the status changes the mode in
                               // threadMain (BBC) function
    {
        return "InitializeDone";
    }
    else if(buffer.substr(0, 5) == "Start") // Changing the status changes the
                                            // mode in threadMain (BBC)
                                            // function
    {
        return "StartDone";
    }
    else if(buffer.substr(0, 4) == "Stop")
    {
        return "StopDone";
    }
    else if(buffer.substr(0, 4) == "Halt")
    {
        return "HaltDone";
    }
    else if(buffer == "Pause")
    {
        return "PauseDone";
    }
    else if(buffer == "Resume")
    {
        return "ResumeDone";
    }
    else if(buffer.substr(0, 9) == "Configure")
    {
        return "ConfigureDone";
    }
    else if(buffer.substr(0, 5) == "K2410")
    {
        std::cout << "Special handling of K2410" << std::endl;
        std::string command = getVariableValue("K2410", buffer);

        std::string powerSupplyId = getVariableValue("PowerSupplyId", buffer);
        std::string channelId     = getVariableValue("ChannelId", buffer);
        auto        channelK2410  = static_cast<KeithleyChannel*>(fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId));

        if(command == "setupIsense")
        {
            float currentCompliance = std::stof(getVariableValue("CurrCompl", buffer));

            channelK2410->setVoltageMode();
            channelK2410->setVoltage(0.0);
            channelK2410->setCurrentCompliance(currentCompliance);
        }
        else if(command == "setupVsense")
        {
            float voltageCompliance = std::stof(getVariableValue("VoltCompl", buffer));

            channelK2410->setCurrentMode();
            channelK2410->setCurrent(0.0);
            channelK2410->setParameter("Isrc_range", (float)1e-6);
            channelK2410->setVoltageCompliance(voltageCompliance);
        }
        else if(command == "setupIsource")
        {
            float current           = std::stof(getVariableValue("Current", buffer));
            float voltageCompliance = std::stof(getVariableValue("VoltCompl", buffer));

            channelK2410->setCurrentMode();
            channelK2410->setCurrent(current);
            channelK2410->setVoltageCompliance(voltageCompliance);
        }
        else if(command == "setupVsource")
        {
            float voltage           = std::stof(getVariableValue("Voltage", buffer));
            float currentCompliance = std::stof(getVariableValue("CurrCompl", buffer));

            channelK2410->setVoltageMode();
            channelK2410->setVoltage(voltage);
            channelK2410->setCurrentCompliance(currentCompliance);
        }
        else if(command == "SetVoltage")
        {
            std::string powerSupplyId = getVariableValue("PowerSupplyId", buffer);
            std::string channelId     = getVariableValue("ChannelId", buffer);
            float       voltage       = std::stof(getVariableValue(",Voltage", buffer)); // "," important otherwise will find "SetVoltage" as variable

            std::cout << "Setting voltage to " << std::to_string(voltage) << " V for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

            channelK2410->setParameter("Vsrc_range", getVoltageRange(voltage));
            channelK2410->setVoltage(voltage);
        }
        else
        {
            std::cerr << __PRETTY_FUNCTION__ << " Couldn't recognige K2410 command: " << buffer << ". Aborting..." << std::endl;
            return "Didn't understand the K2410 command!";
        }

        return "K2410 done";
    }
    else if(valuesMap["Header"] == "GetDeviceConnected")
    {
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        replayMessage += ",ChannelList:{";
        for(const auto& readoutChannel: fHandler.getReadoutList()) { replayMessage += readoutChannel + ","; }
        replayMessage.erase(replayMessage.size() - 1);
        replayMessage += "}";
        return replayMessage;
    }
    else if(valuesMap["Header"]  == "TurnOn")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
	std::cout << fHandler.getPowerSupply(powerSupplyId) << std::endl;
	auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->turnOn();
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " is On : " << channel->isOn() << std::endl;
        return "TurnOnDone";
    }
    else if(valuesMap["Header"] == "TurnOff")
    {	

        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
 	auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->turnOff();
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " is On : " << channel->isOn() << std::endl;
        return "TurnOffDone";
    }
    else if(valuesMap["Header"] == "GetStatus")
    {
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        std::string replayMessage;
        replayMessage += "TimeStamp:" + getTimeStamp();
        for(const auto& readoutChannel: fHandler.getStatus()) { replayMessage += ("," + readoutChannel); }
        return replayMessage;
    }
    else if(valuesMap["Header"] == "SetVoltage")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       voltage       = std::stof(valuesMap["Voltage"]); // "," important otherwise will find "SetVoltage" as variable
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        std::cout << "Setting voltage to " << std::to_string(voltage) << " V for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

        fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->setVoltage(voltage);

        return "SetVoltageDone";
    }
    else if (valuesMap["Header"] == "GetVoltage") //(buffer.substr(0, 10) == "GetVoltage")
    {

        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        float voltage = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->getOutputVoltage();

        std::cout << "Got voltage = " << std::to_string(voltage) << " V from Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;
        return std::to_string(voltage);
    }
    else if(valuesMap["Header"] == "SetCurrent")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       current       = std::stof(valuesMap["Current"]); // "," important otherwise will find "SetVoltage" as variable

        std::cout << "Setting current to " << std::to_string(current) << " A for Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;

        fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->setCurrent(current);

        return "SetCurrentDone";
    }
    else if(valuesMap["Header"] == "GetCurrent") // (buffer.substr(0, 10) == "GetCurrent")
    { 
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
        float current = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId)->getCurrent();
        std::cout << "Got current = " << std::to_string(current) << " A from Power supply = " << powerSupplyId << " ChannelId = " << channelId << std::endl;
        return std::to_string(current);
    }
    else if(valuesMap["Header"] == "RunITIVSLDOScan") // IV curve testing message
    {
        std::string configFile = valuesMap["configFile"];
        std::string cmdLine    = "ITIVCurve -s -c " + configFile;
        std::cout << "Executing the following command line: " << cmdLine << std::endl;
        int v = system(cmdLine.c_str());
        std::cout << "Execution status is: " << v << std::endl;
        return "IVITSLDOScanDone";
    }
    else if(valuesMap["Header"] == "initITIVTools")
    {
        const std::string configFile = valuesMap["configFile"];
	bool	    verbose    = false;
	auto	    it	       = valuesMap.find("verbose");
	if (it != valuesMap.end()) std::istringstream(it->second) >> std::boolalpha >> verbose;
	//fITIVToolTest          = new ITIVTools(configFile, true, verbose); // Hardcoded true means you don't want to switch off PS after data acquisition
	//fITIVToolTest->ConfigureSaving();
	//std::cout << fITIVToolTest->GetDirection() << std::endl;
    }
    // else if(buffer.substr(0, 17) == "PrepareMultimeter")
    //{
    //    std::string multimeterId = getVariableValue("multimeterId", buffer);
    //    auto        multimeter   = static_cast<KeithleyMultimeter*>(fHandler.getMultimeter(multimeterId));
    //    multimeter->reset();
    //    multimeter->enableInternalScan();
    //    return "PrepareMultimeterDone";
    //}
    else if(valuesMap["Header"] == "SetVProtection")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       protection    = std::stof(valuesMap["VoltageProtection"]);
	std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " overvoltage protection set to : " << protection << std::endl;
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setOverVoltageProtection(protection);
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " overvoltage protection set to : " << channel->getOverVoltageProtection() << std::endl;
        return "SetOverVoltageProtectionDone";
    }
    else if(valuesMap["Header"] == "SetVCompliance")
    {
        std::string powerSupplyId = valuesMap["PowerSupplyId"];
        std::string channelId     = valuesMap["ChannelId"];
        float       compliance    = std::stof(valuesMap["VoltageCompliance"]);
	std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " compliance set to : " << compliance << std::endl;
        auto        channel       = fHandler.getPowerSupply(powerSupplyId)->getChannel(channelId);
        channel->setVoltageCompliance(compliance);
        std::cout << "Power supply = " << powerSupplyId << " ChannelId = " << channelId << " compliance set to : " << channel->getVoltageCompliance() << std::endl;
        return "SetVoltageComplianceDone";
    }
    // else if(buffer.substr(0, 20) == "CreateScannerCardMap")
    //{
    //    bool        verbosityFlag   = true;
    //    bool        twoChannelsFlag = true;
    //    std::string multimeterId    = getVariableValue("multimeterId", buffer);
    //    std::string configFileName  = getVariableValue("configFile", buffer);
    //    auto        multimeter      = static_cast<KeithleyMultimeter*>(fHandler.getMultimeter(multimeterId));

    //    fSaveFile = new std::ofstream;
    //    pugi::xml_document     fIVSettings;
    //    pugi::xml_parse_result result = fIVSettings.load_file(configFileName.c_str());
    //    std::cout << "Xml config file loaded: " << result.description() << std::endl;
    //    pugi::xml_node testNode               = fIVSettings.child("IVCurve");
    //    pugi::xml_node saveFileNode           = testNode.child("Save");
    //    pugi::xml_node instrumentNode         = testNode.child("Devices");
    //    pugi::xml_node multimeterReadTypeNode = testNode.child(instrumentNode.child("Multimeter").attribute("ReadType").as_string());
    //    this->ConfigureSaving(&saveFileNode);
    //    multimeter->createScannerCardMap(&multimeterReadTypeNode, fChannelMap, verbosityFlag);
    //    this->PrepareFileHeaderScanner(&multimeterReadTypeNode, fChannelMap, twoChannelsFlag);
    //    fSaveFile->close();
    //    return "CreatingScannerCardMapDone";
    //}
    // else if(buffer.substr(0, 20) == "ReadScannerCardPoint")
    //{
    //    std::regex  e(";");
    //    std::string multimeterId = getVariableValue("multimeterId", buffer);
    //    std::string psRead       = getVariableValue("psRead", buffer);
    //    psRead                   = std::regex_replace(psRead, e, ",");
    //    std::cout << "psRead: " << psRead << std::endl;
    //    auto multimeter = static_cast<KeithleyMultimeter*>(fHandler.getMultimeter(multimeterId));
    //    fSaveFile->open(fSaveFileName, std::ofstream::out | std::ofstream::app);
    //    *fSaveFile << psRead;
    //    *fSaveFile << multimeter->scanChannels(fChannelMap.begin()->first, fChannelMap.end()->first);
    //    fSaveFile->close();
    //}
    // else if(buffer.substr(0, 11) == "RunAnalysis")
    //{
    //    std::string configPath = getVariableValue("configFile", buffer);

    //    pugi::xml_document     fIVSettings;
    //    pugi::xml_parse_result result = fIVSettings.load_file(configPath.c_str());
    //    std::cout << "Xml config file loaded: " << result.description() << std::endl;
    //    pugi::xml_node testNode     = fIVSettings.child("IVCurve");
    //    pugi::xml_node saveFileNode = testNode.child("Save");
    //    bool           Kfactor      = saveFileNode.attribute("Kfactor").as_bool();
    //    std::string    Vfit         = saveFileNode.attribute("Vfit").as_string();
    //    std::string    Minfit       = saveFileNode.attribute("Minfit").as_string();
    //    std::string    Maxfit       = saveFileNode.attribute("Maxfit").as_string();
    //    std::string    RextD        = saveFileNode.attribute("RextD").as_string();
    //    std::string    RextA        = saveFileNode.attribute("RextA").as_string();

    //    bool TwoChannels = true;
    //    if(fSaveFile != NULL && fSaveFile->is_open()) fSaveFile->close();
    //    std::cout << ("Copying " + fSaveFileName + " to " + fSaveDir + "lastScan.csv").c_str() << std::endl;
    //    sleep(1);
    //    boost::filesystem::copy_file(fSaveFileName.c_str(),                              // From
    //                                 (fSaveDir + "lastScan.csv").c_str(),                // to
    //                                 boost::filesystem::copy_option::overwrite_if_exists // Copy options: overwrite if it exist
    //    );
    //    std::cout << "Execution of python script for matplotlib plot of IV" << std::endl;
    //    std::string kFactorValue     = "";
    //    std::string twoChannelsValue = "";
    //    if(Kfactor) kFactorValue = " -k ";
    //    if(TwoChannels) twoChannelsValue = " -t ";
    //    std::string cmd = "python3 scripts/analysisIVscanCROC.py -c" + configPath + " -d " + fSaveDir + " -m1 " + Minfit + " -m2 " + Maxfit + " -r1 " + RextD + " -r2 " + RextA +
    //                      kFactorValue + twoChannelsValue;
    //    std::cout << "Executing: " << cmd << std::endl;
    //    int v = system(cmd.c_str());
    //    std::cout << "System return value after execution " << v << std::endl;

    //    boost::filesystem::copy_file((fSaveDir + "Images/lastScan.png"), (fSaveFileNameImages + ".png").c_str());
    //    if(Kfactor && TwoChannels)
    //    {
    //        boost::filesystem::copy_file((fSaveDir + "Images/lastScan_kfactorD.png"), (fSaveFileNameImages + "_kfactorD.png").c_str());
    //        boost::filesystem::copy_file((fSaveDir + "Images/lastScan_kfactorA.png"), (fSaveFileNameImages + "_kfactorA.png").c_str());
    //    }
    //}
    else if(buffer.substr(0, 6) == "Error:")
    {
        if(buffer == "Error: Connection closed") std::cerr << __PRETTY_FUNCTION__ << buffer << ". Closing client server connection!" << std::endl;
        return "";
    }
    else
    {
        std::cerr << __PRETTY_FUNCTION__ << " Can't recognize message: " << buffer << ". Aborting..." << std::endl;
        abort();
    }

    if(running_ || paused_) // We go through here after start and resume or
                            // pause: sending back current status
    {
        std::cout << "Getting time and status here" << std::endl;
    }

    return "Didn't understand the message!";
}

float ITPowerSupplyInterface::getVoltageRange(float voltage)
{
    if(voltage < (float)0.2)
        return 0.2;
    else if(voltage < 2)
        return 2;
    else if(voltage < 20)
        return 20;
    else
        return 1000;
}

//========================================================================================================================
std::string ITPowerSupplyInterface::getTimeStamp()
{
    time_t     rawtime;
    struct tm* timeinfo;
    char       buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", timeinfo);
    std::string str(buffer);
    return str;
}

// Antonio
/*!
************************************************
 * Utility to split strings based on given
 * separator.
 \param line String to be splitted.
 \param separator Separator character used to
 split strings (default is ',')
 \return Returns a vector with splitted strings.
************************************************
*/
std::vector<std::string> ITPowerSupplyInterface::splitString(const std::string& line, const char& separator)
{
    std::stringstream        buffer(line);
    std::string              aux;
    std::vector<std::string> splittedString;

    while(std::getline(buffer, aux, separator)) splittedString.push_back(aux);
    return splittedString;
}

/*!
************************************************
 * Creates a variable/value map starting from
 * command string and based on a given
 * separator.
 \param line Command input string.
 \param separator Separator used to create
 the map (default value is ':')
 \return Returns a map with variable
 name/variable value.
************************************************
*/
std::map<std::string, std::string> ITPowerSupplyInterface::createVariablesMap(const std::string& line, const char& separator)
{
    std::map<std::string, std::string> variablesMap;
    const std::vector<std::string>     splittedCommandString = splitString(line);
    variablesMap["Header"]                                   = splittedCommandString[0];
    for(unsigned int v = 1; v < splittedCommandString.size(); ++v)
    {
        std::vector<std::string> splitCommand = splitString(splittedCommandString[v], separator);
        if(splitCommand.size() != 2)
        {
            std::stringstream error;
            error << "Bad command variable value formatting for \"" << splittedCommandString[v] << "\" in command line: \"" << line << "\"";
            throw std::runtime_error(error.str());
        }
        const std::string key = eraseFirstBlankCharacter(splitCommand[0]);
        const std::string val = eraseFirstBlankCharacter(splitCommand[1]);
        variablesMap[key]     = val;
    }
    return variablesMap;
}

/*!
************************************************
 * Strips first char from a string if blank.
 \param str String to be transformed.
 \return Transformed string.
************************************************
*/
std::string ITPowerSupplyInterface::eraseFirstBlankCharacter(const std::string& str)
{
    if(str.substr(0, 1) == " ")
        return str.substr(1, str.size());
    else
        return str;
}

//
// void ITPowerSupplyInterface::PrepareFileHeaderScanner(pugi::xml_node* multimeterReadTypeNode, std::map<int, std::string>& channelMap, bool twoChannelsFlag)
//{
//    int nChannels = multimeterReadTypeNode->attribute("nChannels").as_int();
//    if(twoChannelsFlag)
//        *fSaveFile << "CurrentD"
//                   << ",VpsD"
//                   << ",CurrentA"
//                   << ",VpsA";
//    else
//        *fSaveFile << "Current"
//                   << ",Vps";
//    for(int v = 1; v < nChannels + 1; ++v)
//    {
//        if(channelMap.count(v) != 1)
//        {
//            // std::cout << "Entering here: " << v << std::endl;
//            *fSaveFile << ",";
//            if(channelMap.count(v) > 1) std::cout << "More than one channel_" << v << " defined, please check config file" << std::endl;
//            continue;
//        }
//        *fSaveFile << "," << channelMap[v];
//        ;
//    }
//    *fSaveFile << std::endl;
//}
//
// void ITPowerSupplyInterface::ConfigureSaving(pugi::xml_node* saveFileNode)
//{
//    std::string saveDir      = saveFileNode->attribute("path").as_string();
//    std::string saveFileName = saveFileNode->attribute("file").as_string();
//    std::cout << "saveDir: " << saveDir << std::endl;
//    std::cout << "saveFileName: " << saveFileName << std::endl;
//    time_t     rawtime;
//    struct tm* timeinfo;
//    char       dateString[15];
//    char       timeString[15];
//    time(&rawtime);
//    timeinfo = localtime(&rawtime);
//    strftime(dateString, sizeof(dateString), "%Y_%m_%d/", timeinfo);
//    strftime(timeString, sizeof(timeString), "%H_%M_%S_", timeinfo);
//    saveDir += dateString;
//    fSaveDir = saveDir;
//    boost::filesystem::path dir(saveDir.c_str());
//    boost::filesystem::create_directories(dir);
//    fSaveFileName       = saveDir + timeString + saveFileName;
//    fSaveFileNameImages = saveDir + "Images/" + timeString + saveFileName;
//    std::cout << "fSaveFileName: " << fSaveFileName << std::endl;
//    fSaveFile->open(fSaveFileName, std::ofstream::out | std::ofstream::app);
//}
