#include "ArduinoKIRA.h"
#include "EthernetConnection.h"
#include "SerialConnection.h"

#include <iostream>
#include <string>

ArduinoKIRA::ArduinoKIRA(const pugi::xml_node configuration) : Arduino("ArduinoKIRA", configuration)
{
    configure();
}
ArduinoKIRA::~ArduinoKIRA()
{
    if(fConnection != nullptr) delete fConnection;
}

void ArduinoKIRA::configure()
{
    std::cout << "Configuring ArduinoKIRA ..." << std::endl;
    if(std::string(fConfiguration.attribute("Connection").value()).compare("Serial") == 0)
    {
        fConnection = new SerialConnection(fConfiguration.attribute("Port").value(), 9600, true, false, false, "\r\n", "\n", 5);
    }
    else if(std::string(fConfiguration.attribute("Connection").value()).compare("Ethernet") == 0)
    {
        throw std::runtime_error("Shouldn't go via ethernet");
        fConnection = new SharedEthernetConnection(fConfiguration.attribute("IPAddress").value(), std::stoi(fConfiguration.attribute("Port").value()));
    }
    else
    {
        std::stringstream error;
        error << "ArduinoKIRA: Cannot implement connection type " << fConfiguration.attribute("Connection").value() << ".\n"
              << "Possible values are Serial or Ethernet";
        throw std::runtime_error(error.str());
    }
    
    for(pugi::xml_node led = fConfiguration.child("LED"); led; led = led.next_sibling("LED"))
    {
        std::string inUse = led.attribute("InUse").value();
        if(inUse.compare("Yes") != 0 && inUse.compare("yes") != 0) continue;
        std::string id = led.attribute("ID").value();
        std::cout << __PRETTY_FUNCTION__ << " Configuring LED: " << id << std::endl;
        ArduinoKIRA::fLEDMap.emplace(id, new KIRALED(fConnection, led));
        ArduinoKIRA::fLEDList.push_back(id);

    }
    setParameter("trigger",     fConfiguration.child("KIRA").attribute("Trigger").as_string() );
    setParameter("frequency",   fConfiguration.child("KIRA").attribute("TriggerFrequency").as_string() );
    setParameter("dacLed",      fConfiguration.child("KIRA").attribute("DacLed").as_string() );
    setParameter("pulseLength", fConfiguration.child("KIRA").attribute("PulseLength").as_string() );
}

KIRALED* ArduinoKIRA::getLED(const std::string& id)
{
    if(fLEDMap.find(id) == fLEDMap.end())
    {
        throw std::out_of_range("No LED with id " + id + " has been configured!");
    }
    return fLEDMap.find(id)->second;
}

std::vector<std::string> ArduinoKIRA::getLEDList(void) const
{
    return fLEDList;
}


float ArduinoKIRA::getParameterFloat(std::string parameter, std::string component)
{
    std::string cmd = component.empty() ? parameter : parameter + "_" + component;

    std::string answer = fConnection->read(cmd);
    float result;
    sscanf(answer.c_str(), "%f", &result);
    return result;
}

int ArduinoKIRA::getParameterInt(std::string parameter, std::string component)
{
    std::string answer = fConnection->read(parameter);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

bool ArduinoKIRA::getParameterBool(std::string parameter, std::string component)
{
    std::string answer = fConnection->read(parameter);
    int         result;
    sscanf(answer.c_str(), "%d", &result);
    return result;
}

std::string ArduinoKIRA::getParameterString(std::string parameter, std::string component)
{
    //std::cout << parameter << std::endl;
    std::string answer = fConnection->read(parameter);
    //std::cout << answer << std::endl;
    return answer;
}


void ArduinoKIRA::setParameter(std::string parameter, float value, std::string component)
{
    std::string cmd = component.empty() ? parameter + "_" + std::to_string(value) : parameter + "_" + component + "_" + std::to_string(value);
    std::cout << cmd << std::endl;
    fConnection->write(cmd);
}

void ArduinoKIRA::setParameter(std::string parameter, std::string value, std::string component)
{
    if (parameter.compare("led") == 0 && !component.empty() && value.compare("on") == 0 )
    {
        getLED(component)->turnOn();
    }
    else if (parameter.compare("led") == 0 && !component.empty() && value.compare("off") == 0)
    {
        this->getLED(component)->turnOff();
    }
    else
    {
        std::string cmd =  parameter + "_" + value;
        std::cout << cmd << std::endl;
        fConnection->write(cmd);
    }
}

void ArduinoKIRA::setParameter(std::string parameter, int value, std::string component)
{    
    if (parameter.compare("intensity") == 0 && !component.empty())
    {
        this->getLED(component)->setIntensity(value);
    }
    else
    {
        std::string cmd =  parameter + "_" + std::to_string(value);
        std::cout << cmd << std::endl;
        fConnection->write(cmd);
    }
}

void ArduinoKIRA::sendCommand(std::string command) { fConnection->write(command); }
